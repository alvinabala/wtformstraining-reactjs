from datetime import datetime
from flask import Blueprint
from flask import render_template
from flask import session
from flask import redirect
from flask import request
from flask import g
from flask import flash
from ..forms import reg_form
from ..models import user as UserRef
from ..models import item as ItemRef
from ..items import items_db
from functools import wraps

mod = Blueprint('default', __name__)

def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if 'username' not in session:
            return redirect('/login')
        else:
            return func(*args, **kwargs)
    return wrapper

@mod.route('/', methods=['GET', 'POST'])
def index():
    if 'username' not in session:
        return redirect('/login')
    current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    items_list = list(g.itemsdb.getItemsNames())
    items_prices_list = list(g.itemsdb.getItemsPrices())
    cart_items_list = list(g.cartsdb.getCartItemsNames(session['username']))
    cart_items_prices_list = list(g.cartsdb.getCartItemsPrices(session['username']))
    cart_items_quantity_list = list(g.cartsdb.getCartItemsQuantity(session['username']))
    cart_total = g.cartsdb.getCartItemsTotal(session['username'])
    return render_template('default/index.html', server_time=current_time, items_list=items_list, items_prices_list=items_prices_list, cart_items_list=cart_items_list, cart_items_prices_list=cart_items_prices_list, cart_items_quantity_list=cart_items_quantity_list, cart_total=cart_total)

@mod.route('/login', methods=['GET'])
def login_page():
    if 'username' in session:
        return redirect('/')
    return render_template('default/login.html')

@mod.route('/login', methods=['POST'])
def login_submit():
    username = request.form['username']
    password = request.form['password']
    if g.usersdb.getUserWithPassword(username, password).count() > 0:
        session['username'] = username
        return redirect('/')
    else:
        flash('Invalid username and password.', 'signin_failure')
        return redirect('/login')

@mod.route('/logout', methods=['GET'])
def logout_submit():
    session.pop('username', None)
    session.clear()
    return redirect('/')

@mod.route('/register', methods=['GET', 'POST'])
def register():
    form = reg_form.RegistrationForm(request.form)
    if request.method == 'POST' and form.validate():
        user = UserRef.User(form.username.data, form.email.data,
                    form.password.data)
        g.usersdb.createUserUsingUser(user)
        session['username'] = form.username.data
        return redirect('/')
    return render_template('default/signup.html', form=form)

@mod.route('/register_item', methods=['GET', 'POST'])
def register_item():
    form = reg_form.ItemForm(request.form)
    if request.method == 'POST' and form.validate():
        item = ItemRef.Item(form.item_name.data, form.price.data)
        g.itemsdb.createItemUsingItem(item)
        flash('Added to inventory', 'register_item_success')
        return redirect('/')

    return render_template('default/newItem.html', form=form)

@mod.route('/delete_item', methods=['GET', 'POST'])
def delete_item():
    form = reg_form.ItemForm(request.form)
    if request.method == 'POST' and form.validate():
        item = ItemRef.Item(form.item_name.data, form.price.data)
        g.itemsdb.deleteItemUsingItem(item)
        return redirect('/')
    items = g.itemsdb.getItems()
    return render_template('default/deleteItem.html', items=items, form=form)

@mod.route('/items')
def item_list():
    items = g.itemdb.getItems()
    return render_template('default/index.html', items=items)

@mod.route('/add_cart', methods=['GET', 'POST'])
def add_cart():
    form = reg_form.CartForm(request.form)
    if request.method == 'POST' and form.validate():
        cart_item = ItemRef.Item(form.cart_item_name.data, form.cart_price.data)
        cart_quantity = form.cart_quantity.data
        g.cartsdb.addCartItemUsingItem(cart_item, cart_quantity, session['username'])
        flash('Added to cart', 'add_cart_success')
        return redirect('/')
    flash('Invalid quantity.', 'add_cart_failure')
    return redirect('/')

@mod.route('/remove_cart', methods=['GET', 'POST'])
def remove_cart():

    if request.method == 'POST':
        cart_item_name = request.form['cart_item_name']
        g.cartsdb.removeCartItems(cart_item_name, session['username'])
        flash('Removed from cart', 'remove_cart_success')
        return redirect('/')
    return redirect('/')
