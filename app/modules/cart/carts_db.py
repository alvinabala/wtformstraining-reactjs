class CartDB:
    def __init__(self, conn):
        self.conn = conn

    def getCartItem(self, item_name, username):
        return self.conn.find({'item_name':item_name, 'username':username})

    def getCartItems(self,username):
        return self.conn.find({'username':username})

    def getCartItemsNames(self,username):
        return self.conn.find({'username':username}, {'item_name':1,'_id':0})

    def getCartItemsPrices(self,username):
        return self.conn.find({'username':username}, {'price':1,'_id':0})

    def getCartItemsQuantity(self,username):
        return self.conn.find({'username':username},{'quantity':1,'_id':0})    

    def getCartItemsTotal(self,username):
        cart_items = self.conn.find({'username':username})
        cart_total = 0.0
        for cart_item in cart_items:
            cart_total = cart_total + cart_item['price'] * cart_item['quantity']
        return cart_total

    def addCartItem(self, item_name, price, quantity, username):
        old_cart_item = self.conn.find({'item_name':item_name, 'username':username})
        if old_cart_item.count() > 0:
            new_quantity = int(next(old_cart_item)['quantity']) + int(quantity)
            self.conn.update({'item_name':item_name, 'username':username}, {'$set':{'quantity':new_quantity}})
        else:
            self.conn.insert({'item_name':item_name, 'price':price, 'quantity':quantity, 'username':username})

    def addCartItemUsingItem(self, item, quantity, username):
        old_cart_item = self.conn.find({'item_name':item.item_name, 'username':username})
        if old_cart_item.count() > 0:
            new_quantity = int(next(old_cart_item)['quantity']) + int(quantity)
            self.conn.update({'item_name':item.item_name, 'username':username}, {'$set':{'quantity':new_quantity}})
        else:
            self.conn.insert({'item_name':item.item_name, 'price':item.price, 'quantity':quantity, 'username':username})

    def removeCartItems(self, item_name, username):
        check_item = self.conn.find({'item_name':item_name})
        if check_item.count() > 0:
            self.conn.delete_many({'item_name':item_name, 'username':username})
