from flask import Flask
from flask import g

from modules import default
from modules import users
from modules import items
from modules import cart

from pymongo import MongoClient

app = Flask(__name__)

def get_main_db():
    client = MongoClient('mongodb://localhost:27017/')
    maindb = client.postsdb
    return maindb

@app.before_request
def before_request():
    mainDb = get_main_db()
    g.usersdb = users.UserDB(conn=mainDb.users)
    g.itemsdb = items.ItemDB(conn=mainDb.items)
    g.cartsdb = cart.CartDB(conn=mainDb.carts)

@app.teardown_request
def teardown_request(exception):
    postdb = getattr(g, 'postdb', None)
    if postdb is not None:
        postdb.close()
    userdb = getattr(g, 'userdb', None)
    if userdb is not None:
        userdb.close()
    itemdb = getattr(g, 'itemdb', None)
    if itemdb is not None:
        itemdb.close()
    cartdb = getattr(g, 'cartdb', None)
    if itemdb is not None:
        cartdb.close()

app.register_blueprint(default.mod)
