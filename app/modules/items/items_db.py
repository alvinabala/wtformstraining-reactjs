class ItemDB:
    def __init__(self, conn):
        self.conn = conn

    def getItem(self, item_name):
        return self.conn.find({'item_name':item_name})

    def getItems(self):
        return self.conn.find()

    def getItemsNames(self):
        return self.conn.find({},{'item_name':1, '_id':0})

    def getItemsPrices(self):
        return self.conn.find({},{'price':1,'_id':0})
        
    def createItem(self, item_name, price):
        self.conn.insert({'item_name':item_name, 'price':price})

    def createItemUsingItem(self, item):
        self.conn.insert({'item_name':item.item_name, 'price':item.price})

    def deleteItemUsingItem(self, item):
        check_item = self.conn.find({'item_name':item.item_name, 'price':item.price})
        if check_item.count() > 0:
            self.conn.delete_many({'item_name':item.item_name})
        #flash here
