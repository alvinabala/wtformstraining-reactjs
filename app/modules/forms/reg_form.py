from wtforms import Form
from wtforms import BooleanField
from wtforms import TextField
from wtforms import PasswordField
from wtforms import validators
from wtforms import FloatField
from wtforms import IntegerField

class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=25)])
    email = TextField('Email Address', [validators.Length(min=6, max=35)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    accept_tos = BooleanField('I accept the TOS', [validators.Required()])

class ItemForm(Form):
    item_name = TextField('Item name', [validators.Length(min=1)])
    price = FloatField('Item Price', [validators.NumberRange(min=0.01)])

class CartForm(Form):
    cart_item_name = TextField('Cart Item Name', [validators.Length(min=1)])
    cart_price = FloatField('Item Price', [validators.NumberRange(min=0.01)])
    cart_quantity = IntegerField('Quantity', [validators.NumberRange(min=0)])
